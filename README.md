# Ansible Docker Image

This Packer template builds Docker images on various Linux distributions
with Ansible installed (Python minor version / Ansible patch release).

If you need a specific minor version or other configuration, please send me
a message and I will add it to the CI.


# Prerequisites

- Docker
- Packer v1.6.1 or higher (build only).
- Push access to the configured Docker repository (build only).


# Using

- On the command-line: `docker run -it registry.gitlab.com/unimatrixone/docker/ansible:latest`


# Building

Create the file `local.pkr.hcl` with the appropriate values:

```
locals {
  docker_repository = "<replace with your Docker repository>"
}
```

Alternatively, the Docker repository may be provided through the command
line with the `-var 'docker_repository='"<your Docker repository>"'` parameter
(note the quoting).

Sources are defined by `docker.<os name>-latest` and `docker.<os name>-release`,
where the latter builds the Docker image for a specific release. Supported
operating systems are:

- Alpine (`alpine`)

Two special sources may be specified:

- `docker.latest` - Latest releases for Ansible, Python and Alpine.

Sources are specified using the `-only` parameter. **This parameter
must be specified**, else Packer will use the wrong Docker base images
and place the wrong tags on the built images.

Run the following command to build the latest Ansible release for the latest
Python release, on the latest Alpine release.

```
packer build -only docker.latest .
```

Build for a specific Python version on the latest Alpine release:

```
packer build -only docker.alpine-latest -var python_version=3.6  .
```


Build for a specific Alpine release:

```
packer build -only docker.alpine-release\
  -var ansible_version=<replace with Ansible version>\
  -var os_release=3.11\
  .
```


# License

MIT/BSD


# Author information

This template was created by **Cochise Ruhulessin** for the
[Unimatrix One](https://cloud.unimatrixone.io) platform.

- [Send me an email](mailto:cochise.ruhulessin@unimatrixone.io)
- [GitHub](https://github.com/cochiseruhulessin)
- [LinkedIn](https://www.linkedin.com/in/cochise-ruhulessin-0b48358a/)
