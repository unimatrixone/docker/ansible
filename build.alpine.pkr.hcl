variable "docker_repository" { default = null }
variable "ansible_version" { default = "2.9.12" }
variable "python_version" { default = "3.8" }
variable "os_release" { default = "3.12" }


build {

  sources = [
    "source.docker.alpine-release",
    "source.docker.alpine-latest",
    "source.docker.latest",
  ]

  provisioner "file" {
    source = "files/ansible-${var.ansible_version}.tar.gz.sha"
    destination = "/ansible-${var.ansible_version}.tar.gz.sha"
  }

  provisioner "shell" {
    inline = [
      "addgroup -g ${local.runtime_gid} ${local.runtime_grp}",
      "adduser --home ${local.runtime_home} -u ${local.runtime_uid} --ingroup ${local.runtime_grp} --shell /bin/ash ${local.runtime_usr} -D",
      "apk add curl gcc git libffi-dev make musl-dev rsync openssl openssl-dev unzip yaml"
    ]
    only = ["docker.alpine-latest", "docker.alpine-release", "docker.latest"]
  }

  provisioner "shell" {
    inline = [
      "wget https://releases.ansible.com/ansible/ansible-${var.ansible_version}.tar.gz",
      "sha256sum -c ansible-${var.ansible_version}.tar.gz.sha",
      "tar -zxf ansible-${var.ansible_version}.tar.gz",
      "ls ansible*",
      "cd ansible-${var.ansible_version} && python3 setup.py install > /dev/null",
      "ansible --version"
    ]
  }

  provisioner "shell" {
    inline = [
      "apk del gcc musl-dev openssl-dev yaml-dev",
      "rm -rf /var/cache/apk/",
    ]
    only = ["docker.alpine-latest", "docker.alpine-release", "docker.latest"]
  }

  provisioner "shell" {
    inline = [
      "rm -rf /tmp/*",
      "rm -rf /ansible*"
    ]
  }

  post-processors {
    post-processor "docker-tag" {
      repository  = coalesce(var.docker_repository, local.docker_repository)
      tags        = [
        join("-", [var.ansible_version, "python${var.python_version}-alpine${var.os_release}"]),
        join("-", [var.ansible_version, "python${var.python_version}"]),
        join("-", [regex_replace(var.ansible_version, "\\.[\\d]+$", ""), "python${var.python_version}"]),
      ]
      only        = ["docker.alpine-latest", "docker.alpine-release"]
    }

    # It is assumed here that when -only=latest is specified, the user does
    # not override the ansible_version variable.
    post-processor "docker-tag" {
      repository  = coalesce(var.docker_repository, local.docker_repository)
      tags        = [
        regex_replace(var.ansible_version, "\\.[\\d]+$", ""),
        var.ansible_version,
        "latest",
      ]
      only        = ["docker.latest"]
    }

    post-processor "docker-push" {}
  }
}
