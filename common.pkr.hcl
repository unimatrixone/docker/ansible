

locals {
  runtime_usr = "ansible"
  runtime_grp = "drones"
  runtime_uid = 1000
  runtime_gid = 1000
  runtime_home = "/opt/app"
}
