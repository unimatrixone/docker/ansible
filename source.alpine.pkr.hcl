

source "docker" "latest" {
  image   = "python:alpine"
  commit  = true
  changes = [
    "LABEL maintainer=${local.maintainer}",
    "LABEL version=${var.ansible_version}",
    "WORKDIR ${local.runtime_home}",
    "USER ${local.runtime_uid}"
  ]
}


source "docker" "alpine-latest" {
  image   = "python:${var.python_version}-alpine"
  commit  = true
  changes = [
    "LABEL maintainer=${local.maintainer}",
    "LABEL version=${var.ansible_version}",
    "WORKDIR ${local.runtime_home}",
    "USER ${local.runtime_uid}"
  ]
}

source "docker" "alpine-release" {
  image   = "python:${var.python_version}-alpine${var.os_release}"
  commit  = true
  changes = [
    "LABEL maintainer=${local.maintainer}",
    "LABEL version=${var.ansible_version}",
    "WORKDIR ${local.runtime_home}",
    "USER ${local.runtime_uid}"
  ]
}
